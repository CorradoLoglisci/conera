package active;

import weka.core.Instances;
import weka.classifiers.trees.M5P;

class M5PReg extends Regressor{
public M5PReg(Instances trainingSet) throws Exception{
	super(trainingSet);
		
	this.myClassifier = new M5P();
	setConfig();
	mining();
			
	
}


protected void setConfig() throws Exception {
	// 
	String[] options = {"-M",String.valueOf(Math.sqrt(trainingSet.numInstances()))};
	myClassifier.setOptions(options);
}
	
}