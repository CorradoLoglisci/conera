package active;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;


import weka.core.Instance;
import weka.core.Instances;

public class Regressor {
	protected Instances trainingSet;

	protected weka.classifiers.Classifier myClassifier;
	

	Regressor(Instances trainingSet ) {
		this.trainingSet=trainingSet;

	}
	
	Regressor(weka.classifiers.Classifier myClassifier ) {
		this.myClassifier=myClassifier;

	}
	protected List<Double> classify(Instances workingSet) throws Exception {
		LinkedList<Double> predictedWorking=new LinkedList<Double>();
		for (int i = 0; i < workingSet.numInstances(); i++) {
		      Instance curr =  workingSet.instance(i);
		    //  System.out.println(curr.numClasses());
		      Object pred = myClassifier.classifyInstance(curr);
		      predictedWorking.add((Double)pred);
		}
		return predictedWorking;
		      
	}
	
	protected void mining() throws Exception{
		System.out.println("Learning from "+trainingSet.numInstances()+ " examples");
		this.myClassifier.buildClassifier(this.trainingSet);
	}
	
	public void serialize(String filename) throws IOException {
		filename=filename.replace(".","_");
		FileOutputStream outFile = new FileOutputStream(filename+".classifier");
		ObjectOutputStream outStream = new ObjectOutputStream(outFile);
		outStream.writeObject(myClassifier);
		outStream.close();
	}
	
	static public weka.classifiers.Classifier load(String fileName) throws IOException,ClassNotFoundException {
		weka.classifiers.Classifier classifier;
		FileInputStream inFile= new FileInputStream(fileName+".classifier");
		ObjectInputStream inStream =new ObjectInputStream(inFile);
		
		classifier=(weka.classifiers.Classifier)(inStream.readObject());
		inStream.close();
		
		
		return classifier;
	}
}
